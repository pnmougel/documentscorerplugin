# DocScorer Elasticsearch Plugin


This plugin allows to compute the score of a document for a search request without indexing the document.

The score returned by the endpoint is the score that would be returned by the specified search query if the document had been indexed.

## Install

This plugin is available for all Elasticsearch version since 6.7.x up to 8.5.2. I cannot maintain version compatibility for the latest elasticsearch releases but if you are interested, just let me know :)

Download the latest release for your elasticsearch version using the [release compatibility table](doc/release_compatibility.md)

Install the plugin and restart the service

```sh
sudo bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-<ES_VERSION>_1.0.0.zip
sudo service elasticsearch restart
```

To update the plugin you must first remove the previous version

```sh
bin/elasticsearch-plugin remove document_scorer
```

## Why should I use this plugin ?

Well, you probably already know if you get there :) 

Just in case, there is two use cases for this endpoint :
 * **Debug** with the `explain` parameter set to true. It helps understanding how the score is computed for arbitrary documents add the document to the index.
 * **Relative scoring** : See below :) 


## Using this plugin for relative scoring

### The problem

The scores computed by elasticsearch and Lucene do not tell if the returned documents are good match or not w.r.t. a query. 
It only provides an order, i.e., the first document is a better match than the second, but possibly the first document match very poorly.

Another problem: the scores are not bounded. It is not possible to normalize the score with a maximum value.

Consequently, there is no trivial solution using Lucene or Elasticsearch to the question : **Does this document is a good match or not for my query ?**

### The solution

Instead of normalizing against a maximum score, which is not possible for most query, you can get a relative score compared to an **ideal document**.

This **ideal document** is a document that would have the optimal score for your query. Given a query, such document is usually easy to construct.

Please note that this document will not have the **maximum score**, hence the term **relative scoring** instead of **normalized scoring**. You can always get a higher score by repeating the same terms an infinite number of time. 

Without this plugin, to get a relative score you must :
 - Run the search query once
 - Build and index the **ideal document**
 - Refresh the index
 - Ensure that this document will never be returned by other queries
 - Run the search query again to get the score of the ideal document
 - Delete the document
 - Refresh the index (again !)
 - Probably answer questions about why the elasticsearch is SO SLOW !

The objective of this plugin is to avoid messing with your index but still get the score of the ideal document as if it where indexed.
 - Run the search query once
 - Build the **ideal document**
 - Get the score of the ideal document (very fast !)

## Usage

```text
GET /_docscore
POST /_docscore
```
```json
{
	"index": "INDEX",
	"type": "TYPE",
	"query": {}, 
	"document": {},
	"explain": false
}
```

With the following parameters

| Attribute   | Type     | Required | Description           |
|:------------|:---------|:---------|:----------------------|
| `index`     | string   | yes      | Name of the index having the mapping used to indexed the virtual document and the stats used to compute the score. |
| `type`      | string   | no       | For es6 compatibility, the type in the index. Defaults to `_doc`. |
| `query`     | json obj | yes      | The search query |
| `document`  | json obj | yes      | A json object containing the virtual document |
| `explain`   | boolean  | no       | If true, the score explanation will be returned in the response |


The `index` parameter must match exactly **one** index. It is not possible to use wildcards since a single mapping must be used to index the virtual document. 

### Example

Create a sample index

```
PUT /test_docscorer
{
  "mappings": {
    "properties": {
      "field": {
        "type": "text"
      }
    }
  }
}
```

Add test documents

```
POST /test_docscorer/_doc
{
    "field": "the big red tree"
}
POST /test_docscorer/_doc
{
    "field": "a red flower in the tree"
}
```

Get the score

```
POST /_docscore
{
	"index": "test_docscorer",
	"query": {
		"match": {
			"field": "big red dog"
		}
	},
	"document": {
		"field": "a big red dog"
	},
	"explain": true
}
```
## How it work

 1. Create a lucene [MemoryIndex](https://lucene.apache.org/core/4_4_0/memory/org/apache/lucene/index/memory/MemoryIndex.html)
 2. Index the document in the MemoryIndex using the mapping from the index parameter
 3. Create a custom [IndexSearcher](http://lucene.apache.org/core/7_1_0/core/index.html?org/apache/lucene/search/IndexSearcher.html) merging the statistics from the MemoryIndex and the first shard of the provided index
 4. Run the search using the custom IndexSearcher.


## Comparison with `painless/_execute`

The endpoint [`/_scripts/painless/_execute`](https://www.elastic.co/guide/en/elasticsearch/painless/current/painless-execute-api.html) 
allows to compute the score of a document for a query without indexing it. The main difference is that it will not take into account the statistics of the index.


## Limitations

#### `avgFieldLength` stat
 
The `avgFieldLength` stat does not take into account the virtual document fields length. 
In most case it should not be an issue, particularly if the index contains a large number of documents. 

You may get different scores if the virtual document has a field with a length considerably different from the average field length of the index. 

#### Shard selection

Currently, the stats are retrieved only from the **first shard** of the index. 
If the documents are correctly distributed amongst the shard, which is the default behavior, it should not be an issue.

## Future features

 * Enable routing to select the shard used to gather statistics
 * Allow to use search templates with parameter instead of the query field

## Development

A plugin must be built for each elasticsearch version. For example, a plugin built for elasticsearch 7.1.0 will not be compatible version 7.1.1.

To avoid code redundancy, the source required to build the plugins for elasticsearch versions without API changes is found in folders starting with `es<MIN_VERSION>`. 
E.g., folder `es6.7` contains the code compatible with the elasticsearch API for all `6.7.x` and `6.8.x` versions.

Folder `es_builds` contains project definitions for all elasticsearch versions by specifying the source code to use and the elasticsearch version. 

### Build from source

To build for a specific elasticsearch version, you must first ensures that a children pom exists in the folder `es_builds` for this version. They are all named as `pom_${ES_VERSION}.xml`.

If the file already exists, use the default maven commands and specify the children pom file using `-f` parameter.

```sh
# Build the package
mvn -f es_builds/pom_<ES_VERSION>.xml clean package

# Run only the tests
mvn -f es_builds/pom_<ES_VERSION>.xml clean test
```

The **package** command will create the plugin file in the `releases` directory. 

### Add a new elasticsearch version

If the code is already compatible with the new version, you can simply copy a pom file from the `es_builds` folder and update the properties:

 * `elasticsearch.version`: elasticsearch version for which the plugin is built
 * `source.directory`: must match the directory containing the source code compatible with the elasticsearch API

If the code is not compatible with the API of the desired version, you will have to create a new sub project.

#### Resources used to write the plugin

 * https://github.com/codelibs/elasticsearch-plugin-sample
 * http://david.pilato.fr/blog/2016/10/20/creating-elasticsearch-transport-action-updated-for-ga/#disqus_thread
