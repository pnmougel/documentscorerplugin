// import Mustache from 'Mustache';
const Mustache = require('mustache');
const fs = require('fs-extra');
const spawnSync = require('child_process').spawnSync;
const chalk = require('chalk');

const template = fs.readFileSync('pom_template.mustache.xml', 'UTF-8');
const compatibility = JSON.parse(fs.readFileSync('compatibility.json', 'UTF-8'));

const PROJECT_VERSION = '1.0.0';
const POM_FOLDER = 'es_builds';

fs.removeSync(POM_FOLDER);

releases = [];

compatibility.forEach(item => {
    item.es_api_versions.forEach(esVersion => {
        const pomContent = Mustache.render(template, {
            source_directory: item.source_path,
            elasticsearch_version: esVersion,
            project_version: PROJECT_VERSION
        });
        const pomFilePath = `${POM_FOLDER}/pom_${esVersion}.xml`;
        fs.outputFileSync(`../${pomFilePath}`, pomContent);
        console.log(chalk.bold(`Building plugin for elasticsearch ${esVersion}...`));
        runCommand(['-f', pomFilePath, 'clean']);
        runCommand(['-f', pomFilePath, 'compile']);
        runCommand(['-f', pomFilePath, 'test']);
        runCommand(['-f', pomFilePath, 'package', '-DskipTests']);
        console.log('---------\n');
        releases.push({
            esVersion,
            fileName: `docscorer-${esVersion}_${PROJECT_VERSION}.zip`

        })
    });
});

function runCommand(params) {
    console.log(`[${chalk.bold.blue('INFO')}] ${chalk.bold(`mvn ${params.join(' ')}`)}`);
    const res = spawnSync('mvn', params, {cwd: '../', encoding: 'UTF-8'});
    const successLine = res.stdout.split('\n')
        .filter(line => line.indexOf('BUILD SUCCESS') !== -1 || line.indexOf('Tests run') !== -1);
    if(successLine.length === 0) {
        console.error('Build failed')
    } else {
        console.log(successLine.join('\n'))
    }
    if(res.stderr.length !== 0) {
        console.log(`[${chalk.bold.red('ERROR')}] Warning or errors occurred`);
        console.log(res.stderr)
    }
}

const mdTemplate = fs.readFileSync('release_compatibility.mustache.md', 'UTF-8');
const releaseCompatibility = Mustache.render(mdTemplate, { releases });
fs.outputFileSync('../doc/release_compatibility.md', releaseCompatibility);
