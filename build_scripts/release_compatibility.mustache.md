## Releases

Download the plugin release corresponding to your elasticsearch version

To find your elasticsearch version use the following request
```sh
curl -XGET '<ES_HOST>'
```

| Elasticsearch Version   | Plugin release | Command |
|:------------------------|:---------------|:--------|
{{#releases}}
| {{esVersion}} | [{{fileName}}](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/{{fileName}}?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/{{fileName}}` |
{{/releases}}


Install the plugin and restart the service

```sh
sudo bin/elasticsearch-plugin install docscorer-<VERSION>.zip
sudo service elasticsearch restart
```

If you download locally the plugin archive, you must use the `file` protocol. E.g. `bin/elasticsearch-plugin install file://<PATH_TO_ARCHIVE>` 

To update the plugin you must remove the previous version before

```sh
bin/elasticsearch-plugin remove document_scorer
```
