## Releases

Download the plugin release corresponding to your elasticsearch version

To find your elasticsearch version use the following request
```sh
curl -XGET '<ES_HOST>'
```

| Elasticsearch Version   | Plugin release | Command |
|:------------------------|:---------------|:--------|
| 6.7.0 | [docscorer-6.7.0_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-6.7.0_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-6.7.0_1.0.0.zip` |
| 6.7.1 | [docscorer-6.7.1_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-6.7.1_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-6.7.1_1.0.0.zip` |
| 6.7.2 | [docscorer-6.7.2_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-6.7.2_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-6.7.2_1.0.0.zip` |
| 6.8.0 | [docscorer-6.8.0_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-6.8.0_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-6.8.0_1.0.0.zip` |
| 6.8.1 | [docscorer-6.8.1_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-6.8.1_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-6.8.1_1.0.0.zip` |
| 7.0.0 | [docscorer-7.0.0_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.0.0_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.0.0_1.0.0.zip` |
| 7.0.1 | [docscorer-7.0.1_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.0.1_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.0.1_1.0.0.zip` |
| 7.1.0 | [docscorer-7.1.0_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.1.0_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.1.0_1.0.0.zip` |
| 7.1.1 | [docscorer-7.1.1_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.1.1_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.1.1_1.0.0.zip` |
| 7.2.0 | [docscorer-7.2.0_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.2.0_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.2.0_1.0.0.zip` |
| 7.2.1 | [docscorer-7.2.1_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.2.1_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.2.1_1.0.0.zip` |
| 7.3.0 | [docscorer-7.3.0_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.3.0_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.3.0_1.0.0.zip` |
| 7.3.1 | [docscorer-7.3.1_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.3.1_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.3.1_1.0.0.zip` |
| 7.3.2 | [docscorer-7.3.2_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.3.2_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.3.2_1.0.0.zip` |
| 7.4.0 | [docscorer-7.4.0_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.4.0_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.4.0_1.0.0.zip` |
| 7.4.1 | [docscorer-7.4.1_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.4.1_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.4.1_1.0.0.zip` |
| 7.4.2 | [docscorer-7.4.2_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.4.2_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.4.2_1.0.0.zip` |
| 7.5.0 | [docscorer-7.5.0_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.5.0_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.5.0_1.0.0.zip` |
| 7.5.1 | [docscorer-7.5.1_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.5.1_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.5.1_1.0.0.zip` |
| 7.5.2 | [docscorer-7.5.2_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.5.2_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.5.2_1.0.0.zip` |
| 7.6.0 | [docscorer-7.6.0_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.6.0_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.6.0_1.0.0.zip` |
| 7.6.1 | [docscorer-7.6.1_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.6.1_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.6.1_1.0.0.zip` |
| 7.6.2 | [docscorer-7.6.2_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.6.2_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.6.2_1.0.0.zip` |
| 7.7.0 | [docscorer-7.7.0_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.7.0_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.7.0_1.0.0.zip` |
| 7.7.1 | [docscorer-7.7.1_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.7.1_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.7.1_1.0.0.zip` |
| 7.8.0 | [docscorer-7.8.0_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.8.0_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.8.0_1.0.0.zip` |
| 7.8.1 | [docscorer-7.8.1_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.8.1_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.8.1_1.0.0.zip` |
| 7.9.0 | [docscorer-7.9.0_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.9.0_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.9.0_1.0.0.zip` |
| 7.9.1 | [docscorer-7.9.1_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.9.1_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.9.1_1.0.0.zip` |
| 7.9.2 | [docscorer-7.9.2_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.9.2_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.9.2_1.0.0.zip` |
| 7.9.3 | [docscorer-7.9.3_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.9.3_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.9.3_1.0.0.zip` |
| 7.10.0 | [docscorer-7.10.0_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.10.0_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.10.0_1.0.0.zip` |
| 7.10.1 | [docscorer-7.10.1_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.10.1_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-7.10.1_1.0.0.zip` |
| 8.0.0 | [docscorer-8.0.0_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.0.0_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.0.0_1.0.0.zip` |
| 8.0.1 | [docscorer-8.0.1_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.0.1_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.0.1_1.0.0.zip` |
| 8.1.0 | [docscorer-8.1.0_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.1.0_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.1.0_1.0.0.zip` |
| 8.1.1 | [docscorer-8.1.1_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.1.1_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.1.1_1.0.0.zip` |
| 8.1.2 | [docscorer-8.1.2_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.1.2_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.1.2_1.0.0.zip` |
| 8.1.3 | [docscorer-8.1.3_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.1.3_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.1.3_1.0.0.zip` |
| 8.2.0 | [docscorer-8.2.0_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.2.0_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.2.0_1.0.0.zip` |
| 8.2.1 | [docscorer-8.2.1_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.2.1_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.2.1_1.0.0.zip` |
| 8.2.2 | [docscorer-8.2.2_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.2.2_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.2.2_1.0.0.zip` |
| 8.2.3 | [docscorer-8.2.3_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.2.3_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.2.3_1.0.0.zip` |
| 8.3.0 | [docscorer-8.3.0_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.3.0_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.3.0_1.0.0.zip` |
| 8.3.1 | [docscorer-8.3.1_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.3.1_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.3.1_1.0.0.zip` |
| 8.3.2 | [docscorer-8.3.2_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.3.2_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.3.2_1.0.0.zip` |
| 8.3.3 | [docscorer-8.3.3_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.3.3_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.3.3_1.0.0.zip` |
| 8.4.0 | [docscorer-8.4.0_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.4.0_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.4.0_1.0.0.zip` |
| 8.4.1 | [docscorer-8.4.1_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.4.1_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.4.1_1.0.0.zip` |
| 8.4.2 | [docscorer-8.4.2_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.4.2_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.4.2_1.0.0.zip` |
| 8.4.3 | [docscorer-8.4.3_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.4.3_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.4.3_1.0.0.zip` |
| 8.5.0 | [docscorer-8.5.0_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.5.0_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.5.0_1.0.0.zip` |
| 8.5.1 | [docscorer-8.5.1_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.5.1_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.5.1_1.0.0.zip` |
| 8.5.2 | [docscorer-8.5.2_1.0.0.zip](https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.5.2_1.0.0.zip?inline=false) | `bin/elasticsearch-plugin install https://gitlab.com/pnmougel/documentscorerplugin/raw/master/releases/docscorer-8.5.2_1.0.0.zip` |


Install the plugin and restart the service

```sh
sudo bin/elasticsearch-plugin install docscorer-<VERSION>.zip
sudo service elasticsearch restart
```

If you download locally the plugin archive, you must use the `file` protocol. E.g. `bin/elasticsearch-plugin install file://<PATH_TO_ARCHIVE>` 

To update the plugin you must remove the previous version before

```sh
bin/elasticsearch-plugin remove document_scorer
```
