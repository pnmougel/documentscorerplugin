package org.pnmougel.elasticsearch.plugin.documentscorer;

import org.apache.lucene.index.*;
import org.apache.lucene.search.CollectionStatistics;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.TermStatistics;

import java.io.IOException;
import java.util.concurrent.ExecutorService;

public class CustomIndexSearcher extends IndexSearcher {
    private IndexSearcher inMemorySearcher;
    private IndexSearcher statSearcher;

    private IndexReader inMemoryReader;
    private IndexReader statsReader;

    CustomIndexSearcher(IndexSearcher inMemorySearcher, IndexSearcher statSearcher) {
        this(inMemorySearcher, statSearcher, null);
    }

    CustomIndexSearcher(IndexSearcher inMemorySearcher, IndexSearcher statSearcher, ExecutorService executor) {
        this(inMemorySearcher, statSearcher, inMemorySearcher.getIndexReader().getContext(), executor);
    }

    CustomIndexSearcher(IndexSearcher inMemorySearcher, IndexSearcher statSearcher, IndexReaderContext context, ExecutorService executor) {
        super(context, executor);
        this.inMemorySearcher = inMemorySearcher;
        this.statSearcher = statSearcher;

        this.inMemoryReader = inMemorySearcher.getIndexReader();
        this.statsReader = statSearcher.getIndexReader();
    }

    @Override
    public TermStatistics termStatistics(Term term, TermContext context) throws IOException {
        long docFreq = getSum(statsReader.docFreq(term), inMemoryReader.docFreq(term));
        long totalTermFreq = getSum(statsReader.totalTermFreq(term), inMemoryReader.totalTermFreq(term));

        return new TermStatistics(term.bytes(), docFreq, totalTermFreq);
    }

    /**
     * Return the sum of the collection statistics
     */
    @Override
    public CollectionStatistics collectionStatistics(String field) throws IOException {
        CollectionStatistics stats1 = statSearcher.collectionStatistics(field);
        CollectionStatistics stats2 = inMemorySearcher.collectionStatistics(field);

        assert field != null;

        Terms stat1Terms = MultiFields.getTerms(statsReader, field);
        Terms stat2Terms = MultiFields.getTerms(inMemorySearcher.getIndexReader(), field);

        long maxDoc = (stats1 == null ? 0 : stats1.maxDoc()) + (stats2 == null ? 0 : stats2.maxDoc());
        long docCount = (stat1Terms == null ? 0 : stat1Terms.getDocCount()) + (stat2Terms == null ? 0 : stat2Terms.getDocCount());

        long sumTotalTermFreq1 = stat1Terms == null ? -1 : stat1Terms.getSumTotalTermFreq();
        long sumTotalTermFreq2 = stat2Terms == null ? -1 : stat2Terms.getSumTotalTermFreq();
        long sumTotalTermFreq = getSum(sumTotalTermFreq1, sumTotalTermFreq2);

        long sumDocFreq1 = stat1Terms == null ? -1 : stat1Terms.getSumDocFreq();
        long sumDocFreq2 = stat2Terms == null ? -1 : stat2Terms.getSumDocFreq();
        long sumDocFreq = getSum(sumDocFreq1, sumDocFreq2);

        return new CollectionStatistics(field, maxDoc, docCount, sumTotalTermFreq, sumDocFreq);
    }

    private long getSum(long v1, long v2) {
        if(v1 == -1 || v2 == -1) {
            return -1;
        } else {
            return v1 + v2;
        }
    }
}
