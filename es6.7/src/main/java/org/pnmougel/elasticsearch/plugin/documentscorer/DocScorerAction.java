package org.pnmougel.elasticsearch.plugin.documentscorer;

import org.elasticsearch.action.Action;
import org.elasticsearch.client.ElasticsearchClient;

public class DocScorerAction extends Action<DocScorerRequest, DocScorerResponse, DocScorerBuilder> {

    public static final DocScorerAction INSTANCE = new DocScorerAction();
    public static final String NAME = "cluster:admin/docscorer";

    private DocScorerAction() {
        super(NAME);
    }

    @Override
    public DocScorerResponse newResponse() {
        return new DocScorerResponse();
    }

    @Override
    public DocScorerBuilder newRequestBuilder(ElasticsearchClient elasticsearchClient) {
        return new DocScorerBuilder(elasticsearchClient, INSTANCE);
    }
}
