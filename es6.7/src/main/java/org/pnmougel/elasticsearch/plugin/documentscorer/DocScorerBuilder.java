package org.pnmougel.elasticsearch.plugin.documentscorer;

import org.elasticsearch.action.Action;
import org.elasticsearch.action.ActionRequestBuilder;
import org.elasticsearch.client.ElasticsearchClient;

public class DocScorerBuilder extends ActionRequestBuilder<DocScorerRequest, DocScorerResponse, DocScorerBuilder> {
    public DocScorerBuilder(ElasticsearchClient client, Action<DocScorerRequest, DocScorerResponse, DocScorerBuilder> action) {
        super(client, action, new DocScorerRequest());
    }
}
