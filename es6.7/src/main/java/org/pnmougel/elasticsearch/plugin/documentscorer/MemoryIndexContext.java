package org.pnmougel.elasticsearch.plugin.documentscorer;

import org.apache.lucene.index.IndexableField;
import org.apache.lucene.index.memory.MemoryIndex;
import org.elasticsearch.index.IndexService;
import org.elasticsearch.index.mapper.ParseContext;

class MemoryIndexContext extends VirtualIndexContext {
    MemoryIndexContext(IndexService indexService, DocScorerRequest request) {
        super(indexService, request);
        MemoryIndex index = new MemoryIndex(true);
        for(ParseContext.Document doc : getDocuments()) {
            for(IndexableField field : doc.getFields()) {
                index.addField(field, getAnalyzer());
            }
        }

        this.memorySearcher = index.createSearcher();
        buildShardContext(memorySearcher.getIndexReader());
    }
}
