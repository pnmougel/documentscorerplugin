package org.pnmougel.elasticsearch.plugin.documentscorer;

import org.elasticsearch.action.Action;

public class DocScorerAction extends Action<DocScorerResponse> {

    public static final DocScorerAction INSTANCE = new DocScorerAction();
    public static final String NAME = "cluster:admin/docscorer";

    private DocScorerAction() {
        super(NAME);
    }

    @Override
    public DocScorerResponse newResponse() {
        return new DocScorerResponse();
    }
}
