package org.pnmougel.elasticsearch.plugin.documentscorer;

import org.elasticsearch.client.node.NodeClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.rest.BaseRestHandler;
import org.elasticsearch.rest.RestController;
import org.elasticsearch.rest.RestRequest;
import org.elasticsearch.rest.action.RestToXContentListener;

import java.io.IOException;

import static org.elasticsearch.rest.RestRequest.Method.GET;
import static org.elasticsearch.rest.RestRequest.Method.POST;

public class DocScorerHandler extends BaseRestHandler {

    public DocScorerHandler(final Settings settings,
                            final RestController controller) {
        super(settings);
        controller.registerHandler(GET, "/_docscore", this);
        controller.registerHandler(POST, "/_docscore", this);
    }

    @Override
    public String getName() {
        return "_docscore";
    }

    @Override
    protected RestChannelConsumer prepareRequest(final RestRequest restRequest,
                                                 final NodeClient client) throws IOException {

        DocScorerRequest request = DocScorerRequest.parse(restRequest.contentParser());
        return channel -> client.execute(DocScorerAction.INSTANCE, request, new RestToXContentListener<>(channel));
    }

}
