package org.pnmougel.elasticsearch.plugin.documentscorer;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexReaderContext;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermStates;
import org.apache.lucene.search.CollectionStatistics;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.TermStatistics;

import java.io.IOException;
import java.util.concurrent.ExecutorService;

public class CustomIndexSearcher extends IndexSearcher {
    private IndexSearcher inMemorySearcher;
    private IndexSearcher statSearcher;

    private IndexReader inMemoryReader;
    private IndexReader statsReader;

    CustomIndexSearcher(IndexSearcher inMemorySearcher, IndexSearcher statSearcher) {
        this(inMemorySearcher, statSearcher, null);
    }

    CustomIndexSearcher(IndexSearcher inMemorySearcher, IndexSearcher statSearcher, ExecutorService executor) {
        this(inMemorySearcher, statSearcher, inMemorySearcher.getIndexReader().getContext(), executor);
    }

    CustomIndexSearcher(IndexSearcher inMemorySearcher, IndexSearcher statSearcher, IndexReaderContext context, ExecutorService executor) {
        super(context, executor);
        this.inMemorySearcher = inMemorySearcher;
        this.statSearcher = statSearcher;

        this.inMemoryReader = inMemorySearcher.getIndexReader();
        this.statsReader = statSearcher.getIndexReader();
    }

    @Override
    public TermStatistics termStatistics(Term term, TermStates context) throws IOException {
        long docFreq = Math.max(1, statsReader.docFreq(term) + inMemoryReader.docFreq(term));
        long totalTermFreq = Math.max(1, statsReader.totalTermFreq(term) + inMemoryReader.totalTermFreq(term));

        return new TermStatistics(term.bytes(), docFreq, totalTermFreq);
    }

    /**
     * Return the sum of the collection statistics
     */
    @Override
    public CollectionStatistics collectionStatistics(String field) throws IOException {
        CollectionStatistics stats1 = statSearcher.collectionStatistics(field);
        CollectionStatistics stats2 = inMemorySearcher.collectionStatistics(field);

        long maxDoc = (stats1 == null ? 0 : stats1.maxDoc()) + (stats2 == null ? 0 : stats2.maxDoc());
        long docCount = (stats1 == null ? 0 : stats1.docCount()) + (stats2 == null ? 0 : stats2.docCount());
        long sumTotalTermFreq = (stats1 == null ? 0 : stats1.sumTotalTermFreq()) + (stats2 == null ? 0 : stats2.sumTotalTermFreq());
        long sumDocFreq = (stats1 == null ? 0 : stats1.sumDocFreq()) + (stats2 == null ? 0 : stats2.sumDocFreq());

        return new CollectionStatistics(field, maxDoc, docCount, sumTotalTermFreq, sumDocFreq);
    }
}
