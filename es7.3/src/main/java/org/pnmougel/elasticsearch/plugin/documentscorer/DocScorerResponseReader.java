package org.pnmougel.elasticsearch.plugin.documentscorer;

import org.elasticsearch.common.io.stream.StreamInput;
import org.elasticsearch.common.io.stream.Writeable;

import java.io.IOException;

public class DocScorerResponseReader implements Writeable.Reader<DocScorerResponse> {
    @Override
    public DocScorerResponse read(StreamInput in) throws IOException {
        double score = in.readOptionalDouble();
        return new DocScorerResponse(score);
    }
}
