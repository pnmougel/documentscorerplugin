package org.pnmougel.elasticsearch.plugin.documentscorer;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.IndexSearcher;
import org.elasticsearch.common.bytes.BytesReference;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.IndexService;
import org.elasticsearch.index.mapper.ParseContext;
import org.elasticsearch.index.mapper.ParsedDocument;
import org.elasticsearch.index.mapper.SourceToParse;
import org.elasticsearch.index.query.QueryShardContext;

import java.util.List;

abstract class VirtualIndexContext {
    private IndexService indexService;
    private DocScorerRequest request;

    protected QueryShardContext context;
    QueryShardContext getContext() {
        return context;
    }

    protected IndexSearcher memorySearcher;
    IndexSearcher getMemorySearcher() {
        return memorySearcher;
    }

    private Analyzer analyzer;
    protected Analyzer getAnalyzer() {
        return analyzer;
    }


    VirtualIndexContext(IndexService indexService, DocScorerRequest request) {
        this.indexService = indexService;
        this.request = request;
        this.analyzer = indexService.getIndexAnalyzers().getDefaultIndexAnalyzer();
    }

    List<ParseContext.Document> getDocuments() {
        String indexName = indexService.index().getName();
        BytesReference document = request.getDocument();
        XContentType xContentType = request.getXContentType();
        SourceToParse sourceToParse = new SourceToParse(indexName, request.getType(), "_id", document, xContentType);
        ParsedDocument parsedDocument = indexService.mapperService().documentMapper().parse(sourceToParse);
        return parsedDocument.docs();
    }

    void buildShardContext(IndexReader indexReader) {
        final long absoluteStartMillis = System.currentTimeMillis();
        this.context = indexService.newQueryShardContext(0, indexReader, () -> absoluteStartMillis, null);
    }
}
