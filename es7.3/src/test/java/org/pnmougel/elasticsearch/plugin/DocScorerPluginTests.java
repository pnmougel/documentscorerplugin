package org.pnmougel.elasticsearch.plugin;

import com.google.gson.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codelibs.curl.CurlResponse;
import org.codelibs.elasticsearch.runner.ElasticsearchClusterRunner;
import org.codelibs.elasticsearch.runner.net.EcrCurl;
import org.elasticsearch.node.Node;
import org.junit.jupiter.api.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

import static org.codelibs.elasticsearch.runner.ElasticsearchClusterRunner.newConfigs;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;

public class DocScorerPluginTests {
    private static  Logger log = LogManager.getLogger(DocScorerPluginTests.class);
    private static ElasticsearchClusterRunner runner;

    private Set<String> testsToRun = new HashSet<>();

    // Set to true if you want to keep the explanation results
    private boolean saveExplanation = false;

    @BeforeAll
    static void setUp() throws Exception {
        log.info("Initialising ES cluster");
        log.info("To show the initialisation logs, change 'logger.es.level' property in 'config/log4j2.properties'");
        // create runner instance
        runner = new ElasticsearchClusterRunner();
        // create ES nodes
        runner.onBuild((number, settingsBuilder) -> {
            settingsBuilder.put("http.cors.enabled", true);
            settingsBuilder.put("http.cors.allow-origin", "*");
        }).build(newConfigs()
                .clusterName("es-cl-run" + System.currentTimeMillis())
                .pluginTypes("org.pnmougel.elasticsearch.plugin.DocScorerPlugin")
                .numOfNode(1));

        // wait for yellow status
        runner.ensureYellow();
    }

    @AfterAll
    static void tearDown() throws Exception {
        // close runner
        runner.close();
        // delete all files
        runner.clean();
        log.info("ES cluster closed");
    }

    @TestFactory
    @DisplayName("Run tests from dataset")
    public Stream<DynamicTest> buildDatasetTests() throws Exception {
        List<String> testFolders = new ArrayList<>();

        URL rootUrl = getClass().getResource("/");
        final String[] requiredFiles = {
                "documents.json",
                "index.json",
                "requests.json",
                "config.json"
        };
        Files.walk(Paths.get(rootUrl.toURI()), 1).forEach(path -> {
            File baseFile = path.toFile();
            boolean isValidPath = Files.isDirectory(path);
            if(isValidPath) {
                HashSet<String> files = new HashSet(Arrays.asList(baseFile.list()));
                for(String requiredFileName : requiredFiles) {
                    isValidPath &= files.contains(requiredFileName);
                }
            }
            if(isValidPath) {
                testFolders.add(baseFile.getName());
            }
        });

        return testFolders.stream()
                .filter(t -> testsToRun.size() == 0 || testsToRun.contains(t))
                .map(t -> dynamicTest("Dataset " + t, () -> handleTest(t)));
    }

    private void handleTest(String basePath) throws Exception {
        Logger log = LogManager.getLogger(DocScorerPluginTests.class);
        log.info("");
        log.info("---");
        log.info("Running tests from folder '" + basePath + "'");

        final Node node = runner.node();

        JsonObject config = getAsJsonObject(basePath, "config.json");
        String indexName = config.get("index").getAsString();
        String testDescription = config.get("description").getAsString();
        log.info(testDescription);
        log.info("index " + indexName);
        log.info("---");

        String indexDefinition = getFileContent(basePath, "index.json");

        // Create index
        try (CurlResponse curlResponse = EcrCurl
                .put(node, indexName).header("Content-Type", "application/json").body(indexDefinition).execute()) {
            final String content = curlResponse.getContentAsString();
            assertNotNull(content);
            assertEquals(curlResponse.getHttpStatusCode(), 200);
        }

        // Index documents
        JsonArray documents = getAsJsonArray(basePath, "documents.json");
        documents.forEach(document -> indexDocument(node, indexName, document.toString()));

        // Refresh index
        refreshIndex(node, indexName);

        JsonArray requests = getAsJsonArray(basePath, "requests.json");
        requests.forEach(request -> {
            JsonObject requestObj = request.getAsJsonObject();
            log.info("");

            if(requestObj.has("description")) {
                String requestDescription = requestObj.get("description").getAsString();
                log.info(requestDescription);
                request.getAsJsonObject().remove("description");
            }

            Boolean isExplain = requestObj.has("explain") && requestObj.get("explain").getAsBoolean();

            try (CurlResponse curlResponse = EcrCurl.get(node, "/_docscore")
                    .header("Content-Type", "application/json")
                    .body(request.toString())
                    .execute()) {
                final String content = curlResponse.getContentAsString();
                assertNotNull(content);
                assertEquals(curlResponse.getHttpStatusCode(), 200);

                // Parse content
                JsonObject response = new JsonParser().parse(content).getAsJsonObject();
                assertTrue(response.has("score"), "Missing score field in response");
                if(isExplain) {
                    assertTrue(response.has("_explanation"), "Missing requested field explanation");
                }

                Double score = response.get("score").getAsDouble();
                log.info(" - Found score:    " + score);

                // Index the test document
                String document = requestObj.get("document").toString();
                String docId = indexDocument(node, indexName, document);
                refreshIndex(node, indexName);

                // Perform the search request
                request.getAsJsonObject().remove("document");
                request.getAsJsonObject().remove("index");
                JsonObject searchResponse = search(node, indexName, request.toString());
                JsonObject hits = searchResponse.get("hits").getAsJsonObject();
                Double maxScore = hits.get("max_score").getAsDouble();
                log.info(" - Expected score: " + maxScore);
                assertEquals(maxScore, score, Math.max(score, maxScore) * 0.1);

                // Log the explanation
                if(isExplain) {
                    Gson prettyGson = new GsonBuilder().setPrettyPrinting().create();
                    JsonObject baseExplanation = response.get("_explanation").getAsJsonObject();
                    if(baseExplanation.get("matched").getAsBoolean()) {
                        JsonObject docExplanation = baseExplanation.get("explanation").getAsJsonObject();
                        if(saveExplanation) {
                            FileWriter fw = new FileWriter("explanation_docscorer.json");
                            prettyGson.toJson(docExplanation, fw);
                            fw.flush();
                        }
                    }
                    JsonArray hitsArray = hits.get("hits").getAsJsonArray();
                    if(hitsArray.size() > 0) {
                        JsonObject searchExplanation = hitsArray.get(0).getAsJsonObject().get("_explanation").getAsJsonObject();
                        if(saveExplanation) {
                            FileWriter fw = new FileWriter("explanation_search.json");
                            prettyGson.toJson(searchExplanation, fw);
                            fw.flush();
                        }
                    }
                }

                deleteDoc(node, indexName, docId);
                refreshIndex(node, indexName);
            } catch (IOException e) {}
        });
    }

    private String getFileContent(String basePath, String fileName) throws Exception {
        URL url = getClass().getResource("/" + basePath + "/" + fileName);
        Path resPath = Paths.get(url.toURI());
        return new String(Files.readAllBytes(resPath), "UTF8");
    }

    private JsonArray getAsJsonArray(String basePath, String fileName) throws Exception {
        String json = getFileContent(basePath, fileName);
        return new JsonParser().parse(json).getAsJsonArray();
    }

    private JsonObject getAsJsonObject(String basePath, String fileName) throws Exception {
        String json = getFileContent(basePath, fileName);
        return new JsonParser().parse(json).getAsJsonObject();
    }

    private String indexDocument(Node node, String indexName, String document) {
        String docId = null;
        try (CurlResponse curlResponse = EcrCurl
                .post(node, indexName + "/_doc")
                .body(document)
                .header("Content-Type", "application/json")
                .execute()) {
            final String content = curlResponse.getContentAsString();
            assertNotNull(content);
            assertEquals(curlResponse.getHttpStatusCode(), 201);

            JsonObject response = new JsonParser().parse(content).getAsJsonObject();
            docId = response.get("_id").getAsString();
        } catch (IOException e) {}
        return docId;
    }

    private void refreshIndex(Node node, String indexName) {
        try (CurlResponse curlResponse = EcrCurl.post(node, indexName + "/_refresh")
                .execute()) {
            final String content = curlResponse.getContentAsString();
            assertNotNull(content);
            assertEquals(curlResponse.getHttpStatusCode(), 200);
        } catch (IOException e) {}
    }

    private JsonObject search(Node node, String indexName, String searchQuery) {
        JsonObject response = null;
        try (CurlResponse curlResponse = EcrCurl
                .get(node, indexName + "/_search")
                .body(searchQuery)
                .header("Content-Type", "application/json")
                .execute()) {
            final String content = curlResponse.getContentAsString();
            assertNotNull(content);
            assertEquals(curlResponse.getHttpStatusCode(), 200);
            response = new JsonParser().parse(content).getAsJsonObject();
        } catch (IOException e) {}
        return response;
    }

    private void deleteDoc(Node node, String indexName, String docId) {
        try (CurlResponse curlResponse = EcrCurl
                .delete(node, indexName + "/_doc/" + docId)
                .execute()) {
            final String content = curlResponse.getContentAsString();
            assertNotNull(content);
            assertEquals(curlResponse.getHttpStatusCode(), 200);
        } catch (IOException e) {}
    }
}
