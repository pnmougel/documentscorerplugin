package org.pnmougel.elasticsearch.plugin.documentscorer;

import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.search.*;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.explain.ExplainResponse;
import org.elasticsearch.action.support.ActionFilters;
import org.elasticsearch.action.support.HandledTransportAction;
import org.elasticsearch.action.support.IndicesOptions;
import org.elasticsearch.cluster.ClusterState;
import org.elasticsearch.cluster.metadata.IndexNameExpressionResolver;
import org.elasticsearch.cluster.service.ClusterService;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.index.Index;
import org.elasticsearch.index.IndexService;
import org.elasticsearch.index.engine.Engine;
import org.elasticsearch.index.shard.IndexShard;
import org.elasticsearch.indices.IndicesService;
import org.elasticsearch.tasks.Task;
import org.elasticsearch.threadpool.ThreadPool;
import org.elasticsearch.transport.TransportService;

import java.io.IOException;

public class DocScorerTransportAction extends HandledTransportAction<DocScorerRequest, DocScorerResponse> {
    private final IndicesService indicesServices;
    private final ClusterService clusterService;

    private final IndexNameExpressionResolver indexNameExpressionResolver;

    @Inject
    public DocScorerTransportAction(Settings settings, ThreadPool threadPool, ActionFilters actionFilters,
                                    IndexNameExpressionResolver resolver, TransportService transportService,
                                    ClusterService clusterService,
                                    IndexNameExpressionResolver indexNameExpressionResolver,
                                    IndicesService indicesServices) {
        super(DocScorerAction.NAME, transportService, actionFilters, DocScorerRequest::new);
        this.indicesServices = indicesServices;
        this.clusterService = clusterService;
        this.indexNameExpressionResolver = indexNameExpressionResolver;
    }

    @Override
    protected void doExecute(Task task, DocScorerRequest request, ActionListener<DocScorerResponse> listener) {
        // build index service
        ClusterState clusterState = clusterService.state();
        IndicesOptions indicesOptions = IndicesOptions.strictSingleIndexNoExpandForbidClosed();
        String indexExpression = request.getIndex();
        Index[] concreteIndices =
                indexNameExpressionResolver.concreteIndices(clusterState, indicesOptions, indexExpression);
        if (concreteIndices.length != 1) {
            throw new IllegalArgumentException("[" + indexExpression + "] does not resolve to a single index");
        }
        Index concreteIndex = concreteIndices[0];
        IndexService indexService = indicesServices.indexServiceSafe(concreteIndex);

        // Load existing shard
        IndexShard shard = indexService.iterator().next();
        Engine.Searcher engineSearcher = shard.acquireSearcher("doc_stats");

        try {
            // Create virtual index
            VirtualIndexContext indexContext = new MemoryIndexContext(indexService, request);
            IndexSearcher searcher = new CustomIndexSearcher(indexContext.getMemorySearcher(), engineSearcher);

            // Use the similarity from the source
            searcher.setSimilarity(engineSearcher.getSimilarity());

            // Build query
            Query q = request.getQuery().toQuery(indexContext.getContext());
            q = engineSearcher.rewrite(q);

            Weight w = searcher.createWeight(q, ScoreMode.TOP_SCORES, 1f);
            LeafReaderContext leafReaderContext = searcher.getIndexReader().leaves().get(0);
            Scorer scorer = w.scorer(leafReaderContext);

            // Get score and build response
            DocScorerResponse response = new DocScorerResponse();
            if(scorer == null) {
                // Document does not match request
                response.setScore(0);
                if(request.getExplain()) {
                    response.setExplainResponse(new ExplainResponse(indexService.index().getName(),
                            request.getType(), "virtual_id", false));
                }
            } else {
                scorer.iterator().nextDoc();
                response.setScore(scorer.score());

                if(request.getExplain()) {
                    // Add scoring explanation
                    Explanation explanation = searcher.explain(q, 0);
                    response.setExplainResponse(new ExplainResponse(indexService.index().getName(),
                            request.getType(), "virtual_id", true, explanation));
                }
            }
            listener.onResponse(response);
        } catch (IOException e) {
            listener.onFailure(e);
        } finally {
            engineSearcher.close();
        }
    }
}
