package org.pnmougel.elasticsearch.plugin.documentscorer;

import org.elasticsearch.action.ActionResponse;
import org.elasticsearch.action.explain.ExplainResponse;
import org.elasticsearch.common.io.stream.StreamInput;
import org.elasticsearch.common.io.stream.StreamOutput;
import org.elasticsearch.common.xcontent.ToXContentObject;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.index.query.QueryBuilder;

import java.io.IOException;

public class DocScorerResponse extends ActionResponse implements ToXContentObject {

    private double score;
    private ExplainResponse explainResponse;

    public void setExplainResponse(ExplainResponse explainResponse) {
        this.explainResponse = explainResponse;
    }

    public DocScorerResponse() {
        this(0);
    }

    public DocScorerResponse(double score) {
        this.score = score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    @Override
    public void writeTo(StreamOutput out) throws IOException {
        // super.writeTo(out);
        out.writeOptionalDouble(score);
    }

    @Override
    public XContentBuilder toXContent(XContentBuilder builder, Params params) throws IOException {
        builder.startObject();
        builder.field("score", score);
        if(explainResponse != null) {
            builder.field("_explanation", explainResponse);
        }
        return builder.endObject();
    }
}
