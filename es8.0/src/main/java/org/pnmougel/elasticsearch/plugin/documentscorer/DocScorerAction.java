package org.pnmougel.elasticsearch.plugin.documentscorer;

import org.elasticsearch.action.ActionType;

public class DocScorerAction extends ActionType<DocScorerResponse> {
    public static final DocScorerAction INSTANCE = new DocScorerAction();
    public static final String NAME = "cluster:admin/docscorer";

    private DocScorerAction() {
        super(DocScorerAction.NAME, new DocScorerResponseReader());
    }
}
