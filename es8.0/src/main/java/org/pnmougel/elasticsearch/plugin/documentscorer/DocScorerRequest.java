package org.pnmougel.elasticsearch.plugin.documentscorer;


import org.elasticsearch.action.ActionRequest;
import org.elasticsearch.action.ActionRequestValidationException;
import org.elasticsearch.common.bytes.BytesReference;
import org.elasticsearch.common.io.stream.StreamInput;
import org.elasticsearch.common.io.stream.StreamOutput;
import org.elasticsearch.common.xcontent.*;
import org.elasticsearch.index.query.AbstractQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.xcontent.*;

import java.io.IOException;
import java.util.Objects;

public class DocScorerRequest extends ActionRequest implements ToXContentObject {
    private static final ParseField INDEX_FIELD = new ParseField("index");
    private static final ParseField TYPE_FIELD = new ParseField("type");
    private static final ParseField DOCUMENT_FIELD = new ParseField("document");
    private static final ParseField QUERY_FIELD = new ParseField("query");
    private static final ParseField EXPLAIN_FIELD = new ParseField("explain");

    private static final ConstructingObjectParser<DocScorerRequest, Void> PARSER =
            new ConstructingObjectParser<>("doc_scorer_context",
                    args -> new DocScorerRequest((String) args[0], (String) args[1], (BytesReference) args[2], (QueryBuilder) args[3], (Boolean) args[4]));

    static DocScorerRequest parse(XContentParser parser) throws IOException {
        return PARSER.parse(parser, null);
    }

    static {
        PARSER.declareString(ConstructingObjectParser.constructorArg(), INDEX_FIELD);
        PARSER.declareString(ConstructingObjectParser.optionalConstructorArg(), TYPE_FIELD);
        PARSER.declareObject(ConstructingObjectParser.constructorArg(), (p, c) -> {
            try (XContentBuilder b = XContentBuilder.builder(p.contentType().xContent())) {
                b.copyCurrentStructure(p);
                return BytesReference.bytes(b);
            }
        }, DOCUMENT_FIELD);
        PARSER.declareObject(ConstructingObjectParser.constructorArg(), (p, c) ->
                AbstractQueryBuilder.parseInnerQueryBuilder(p), QUERY_FIELD);
        PARSER.declareBoolean(ConstructingObjectParser.optionalConstructorArg(), EXPLAIN_FIELD);
    }

    private final String index;
    private final String type;
    private final BytesReference document;
    private final QueryBuilder query;
    private final Boolean explain;

    private XContentType xContentType = XContentType.JSON;


    public DocScorerRequest(String index, String type, BytesReference document, QueryBuilder query, Boolean explain) {
        this.index = index;
        this.type = type;
        this.document = document;
        this.query = query;
        this.explain = explain;
    }

    DocScorerRequest(StreamInput in) throws IOException {
        index = in.readString();
        type = in.readString();
        document = in.readBytesReference();
        query = in.readNamedWriteable(QueryBuilder.class);
        explain = in.readOptionalBoolean();
    }

    public String getIndex() {
        return index;
    }

    public String getType() {
        return this.type == null ? "_doc" : this.type;
    }

    public BytesReference getDocument() {
        return document;
    }

    public QueryBuilder getQuery() {
        return query;
    }

    public XContentType getXContentType() {
        return xContentType;
    }

    public Boolean getExplain() {
        return explain != null && explain;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DocScorerRequest that = (DocScorerRequest) o;
        return Objects.equals(index, that.index) &&
                Objects.equals(type, that.type) &&
                Objects.equals(document, that.document) &&
                Objects.equals(query, that.query) &&
                Objects.equals(explain, that.explain) &&
                Objects.equals(xContentType, that.xContentType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(index, type, document, query, xContentType, explain);
    }

    @Override
    public void writeTo(StreamOutput out) throws IOException {
        out.writeString(index);
        out.writeOptionalString(type);
        out.writeBytesReference(document);
        out.writeString(xContentType.mediaTypeWithoutParameters());
        out.writeNamedWriteable(query);
        out.writeOptionalBoolean(explain);
    }

    @Override
    public String toString() {
        return "DocScorerRequest{" +
                ", index='" + index + '\'' +
                ", type='" + type + '\'' +
                ", document=" + document +
                ", query=" + query +
                ", explain=" + explain +
                ", xContentType=" + xContentType +
                '}';
    }

    @Override
    public XContentBuilder toXContent(XContentBuilder builder, ToXContent.Params params) throws IOException {
        builder.startObject();
        {
            builder.field(INDEX_FIELD.getPreferredName(), index);
            if(type != null) {
                builder.field(TYPE_FIELD.getPreferredName(), type);
            }
            builder.field(DOCUMENT_FIELD.getPreferredName());
            try (XContentParser parser = XContentHelper.createParser(NamedXContentRegistry.EMPTY,
                    LoggingDeprecationHandler.INSTANCE, document, xContentType)) {
                builder.generator().copyCurrentStructure(parser);
            }
            builder.field(QUERY_FIELD.getPreferredName(), query);
            if(explain != null) {
                builder.field(EXPLAIN_FIELD.getPreferredName(), explain);
            }
        }
        return builder.endObject();
    }

    @Override
    public ActionRequestValidationException validate() {
        return null;
    }
}
