package org.pnmougel.elasticsearch.plugin.documentscorer;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.IndexSearcher;
import org.elasticsearch.common.bytes.BytesReference;
import org.elasticsearch.index.IndexService;
import org.elasticsearch.index.mapper.LuceneDocument;
import org.elasticsearch.index.mapper.ParsedDocument;
import org.elasticsearch.index.mapper.SourceToParse;
import org.elasticsearch.index.query.SearchExecutionContext;
import org.elasticsearch.xcontent.XContentType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

abstract class VirtualIndexContext {
    private IndexService indexService;
    private DocScorerRequest request;

    protected SearchExecutionContext context;
    SearchExecutionContext getContext() {
        return context;
    }

    protected IndexSearcher memorySearcher;
    IndexSearcher getMemorySearcher() {
        return memorySearcher;
    }

    private Analyzer analyzer;
    protected Analyzer getAnalyzer() {
        return analyzer;
    }


    VirtualIndexContext(IndexService indexService, DocScorerRequest request) {
        this.indexService = indexService;
        this.request = request;
        this.analyzer = indexService.getIndexAnalyzers().getDefaultIndexAnalyzer();
    }

    List<LuceneDocument> getDocuments() {
        String indexName = indexService.index().getName();
        BytesReference document = request.getDocument();
        XContentType xContentType = request.getXContentType();
        // SourceToParse sourceToParse = new SourceToParse(indexName, request.getType(), "_id", document, xContentType);
        SourceToParse sourceToParse = new SourceToParse(indexName, "_id", document, xContentType);
        ParsedDocument parsedDocument = indexService.mapperService().documentMapper().parse(sourceToParse);
        return parsedDocument.docs();
    }

    void buildShardContext(IndexReader indexReader) {
        final long absoluteStartMillis = System.currentTimeMillis();
        this.context = indexService.newSearchExecutionContext(0, 0, memorySearcher, () -> absoluteStartMillis, null, new HashMap<String, Object>() {
        });
        // this.context = indexService.newQueryShardContext(0, memorySearcher, () -> absoluteStartMillis, null);
    }
}
