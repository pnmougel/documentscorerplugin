package org.pnmougel.elasticsearch.plugin.documentscorer;

import org.elasticsearch.client.internal.node.NodeClient;
import org.elasticsearch.rest.BaseRestHandler;
import org.elasticsearch.rest.RestRequest;
import org.elasticsearch.rest.action.RestToXContentListener;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.elasticsearch.rest.RestRequest.Method.GET;
import static org.elasticsearch.rest.RestRequest.Method.POST;

public class DocScorerHandler extends BaseRestHandler {

    public DocScorerHandler() {
    }

    @Override
    public String getName() {
        return "_docscore";
    }

    @Override
    public List<Route> routes() {
        return Arrays.asList(
                new Route(GET, "/_docscore"),
                new Route(POST, "/_docscore"));
    }

    @Override
    protected RestChannelConsumer prepareRequest(final RestRequest restRequest,
                                                 final NodeClient client) throws IOException {

        DocScorerRequest request = DocScorerRequest.parse(restRequest.contentParser());
        return channel -> client.execute(DocScorerAction.INSTANCE, request, new RestToXContentListener<>(channel));
    }

}
