package org.pnmougel.elasticsearch.plugin.documentscorer;

import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.ByteBuffersDirectory;
import org.elasticsearch.index.IndexService;

import java.io.IOException;

class RAMIndexContext extends VirtualIndexContext {
    RAMIndexContext(IndexService indexService, DocScorerRequest request) throws IOException {
        super(indexService, request);
        ByteBuffersDirectory index = new ByteBuffersDirectory();
        IndexWriter indexWriter = new IndexWriter(index, new IndexWriterConfig(getAnalyzer()));
        indexWriter.addDocuments(getDocuments());
        IndexReader indexReader = DirectoryReader.open(indexWriter);

        buildShardContext(indexReader);
        this.memorySearcher = new IndexSearcher(indexReader);
    }
}
